-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2016 at 03:37 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `group8`
--
CREATE DATABASE IF NOT EXISTS `group8` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `group8`;

-- --------------------------------------------------------

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
CREATE TABLE `auction` (
  `auctionID` int(11) NOT NULL,
  `sellerID` int(11) DEFAULT NULL,
  `auctionStart` datetime DEFAULT NULL,
  `auctionEnd` datetime DEFAULT NULL,
  `reserve` float DEFAULT NULL,
  `status` enum('Active','Cancelled','Ended','Won') NOT NULL,
  `title` char(40) DEFAULT NULL,
  `description` char(255) DEFAULT NULL,
  `winnerID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auction`
--

INSERT INTO `auction` (`auctionID`, `sellerID`, `auctionStart`, `auctionEnd`, `reserve`, `status`, `title`, `description`, `winnerID`) VALUES
(1, 1, '2016-03-15 12:00:00', '2016-03-20 12:00:00', 500, 'Active', 'My Old Socks', 'One pair of old socks', NULL),
(2, 2, '2016-03-15 12:00:00', '2016-03-20 12:00:00', 375.35, 'Active', 'Mystery Box', 'Whatever is in this box', NULL),
(3, 1, '2016-03-15 12:00:00', '2016-03-20 12:00:00', 453.25, 'Cancelled', 'Pocket Lint', 'Pocket lint', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bid`
--

DROP TABLE IF EXISTS `bid`;
CREATE TABLE `bid` (
  `bidID` int(11) NOT NULL,
  `buyerID` int(11) NOT NULL,
  `auctionID` int(11) NOT NULL,
  `bidTime` datetime DEFAULT NULL,
  `bidAmount` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bid`
--

INSERT INTO `bid` (`bidID`, `buyerID`, `auctionID`, `bidTime`, `bidAmount`) VALUES
(1, 1, 2, '2016-03-17 12:00:00', 400),
(2, 2, 2, '2016-03-17 12:35:35', 425.25),
(3, 1, 2, '2016-03-17 13:20:25', 450);

-- --------------------------------------------------------

--
-- Table structure for table `buyer`
--

DROP TABLE IF EXISTS `buyer`;
CREATE TABLE `buyer` (
  `buyerID` int(11) NOT NULL,
  `userName` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer`
--

INSERT INTO `buyer` (`buyerID`, `userName`) VALUES
(1, 'LeisureSuitLarry'),
(2, 'WanderingWendy');

-- --------------------------------------------------------

--
-- Table structure for table `phonenumber`
--

DROP TABLE IF EXISTS `phonenumber`;
CREATE TABLE `phonenumber` (
  `userName` char(20) DEFAULT NULL,
  `phoneNumber` char(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phonenumber`
--

INSERT INTO `phonenumber` (`userName`, `phoneNumber`) VALUES
('EvilAnvik', '14036661354'),
('Al', '14033658654'),
('EvilAnvik', '14035551234'),
('LeisureSuitLarry', '15895236542');

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
CREATE TABLE `seller` (
  `sellerID` int(11) DEFAULT NULL,
  `userName` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`sellerID`, `userName`) VALUES
(1, 'EvilAnvik'),
(2, 'Al');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userName` char(20) NOT NULL,
  `passwd` char(20) NOT NULL,
  `country` char(2) NOT NULL,
  `prov_state` char(15) NOT NULL,
  `addr_unit` char(6) DEFAULT NULL,
  `addr_houseNum` int(11) NOT NULL,
  `addr_street` char(25) NOT NULL,
  `addr_city` char(15) NOT NULL,
  `addr_streetDirection` char(2) DEFAULT NULL,
  `addr_postCode` char(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userName`, `passwd`, `country`, `prov_state`, `addr_unit`, `addr_houseNum`, `addr_street`, `addr_city`, `addr_streetDirection`, `addr_postCode`) VALUES
('Al', '123abc', 'CA', 'Alberta', NULL, 123, 'Easy Street', 'PleasantVille', 'W', 'T1K3J4'),
('EvilAnvik', '123abc', 'CA', 'Alberta', '2', 456, 'New York Avenue', 'Charleston', '', 'Z4JTY3'),
('LeisureSuitLarry', '123abc', 'US', 'California', NULL, 49, 'Hollywood Blvd', 'Hollywood', '', '92345'),
('WanderingWendy', '123abc', 'CA', 'Alverta', '23', 234, '3 Ave', 'Lethbridge', 'S', '2j3k4j');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auction`
--
ALTER TABLE `auction`
  ADD PRIMARY KEY (`auctionID`),
  ADD UNIQUE KEY `auctionID` (`auctionID`),
  ADD KEY `sellerID` (`sellerID`),
  ADD KEY `FK_WinningBid` (`winnerID`);

--
-- Indexes for table `bid`
--
ALTER TABLE `bid`
  ADD PRIMARY KEY (`bidID`,`buyerID`,`auctionID`),
  ADD UNIQUE KEY `bidID` (`bidID`),
  ADD KEY `buyerID` (`buyerID`),
  ADD KEY `auctionID` (`auctionID`);

--
-- Indexes for table `buyer`
--
ALTER TABLE `buyer`
  ADD PRIMARY KEY (`buyerID`,`userName`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `phonenumber`
--
ALTER TABLE `phonenumber`
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`userName`),
  ADD UNIQUE KEY `sellerID` (`sellerID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auction`
--
ALTER TABLE `auction`
  ADD CONSTRAINT `FK_WinningBid` FOREIGN KEY (`winnerID`) REFERENCES `bid` (`buyerID`),
  ADD CONSTRAINT `auction_ibfk_1` FOREIGN KEY (`sellerID`) REFERENCES `seller` (`sellerID`);

--
-- Constraints for table `bid`
--
ALTER TABLE `bid`
  ADD CONSTRAINT `bid_ibfk_1` FOREIGN KEY (`buyerID`) REFERENCES `buyer` (`buyerID`),
  ADD CONSTRAINT `bid_ibfk_2` FOREIGN KEY (`auctionID`) REFERENCES `auction` (`auctionID`);

--
-- Constraints for table `buyer`
--
ALTER TABLE `buyer`
  ADD CONSTRAINT `buyer_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `user` (`userName`);

--
-- Constraints for table `phonenumber`
--
ALTER TABLE `phonenumber`
  ADD CONSTRAINT `phonenumber_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `user` (`userName`);

--
-- Constraints for table `seller`
--
ALTER TABLE `seller`
  ADD CONSTRAINT `seller_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `user` (`userName`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
