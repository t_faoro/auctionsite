<?php 

error_reporting(0);

/**
* Insructions:
*	Include any function files that you write into this file. We will include this file into the index.php
*	and all of the functions will be available for use.
*/

//:: File Path Declarations:
define(CSS, "./css/");
define(JS, "./js/");
define(FUNC, "functions/");

//:: Include Database Connection Class:
include_once "connection/connection.class.php";
//include_once "include/authentication.class.php";


//:: Custom Function Includes:
include_once FUNC."AuctionFunctions.function.php";
include_once FUNC."template.function.php";
include_once FUNC."pageFunctions.function.php";
include_once FUNC."authenticate.function.php";



//::::::THIS LINE MUST BE CHANGED BEFORE SITE IS FINALIZED:::::::::\\
/*******Current Set be statically true until finished*******/
/**/		//$_SESSION['authenticated'] = FALSE;			/**/
/**********************************************************/
//$_SESSION['id'] = NULL;


?>
