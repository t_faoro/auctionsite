<?php 

function deleteAuctionQuery($con, $auctionID){
    $sql  = 'SELECT status FROM AUCTION ';
    $sql .= 'WHERE auctionID = '.$auctionID;
    //echo $sql;

    $result = mysqli_query($con, $sql);
    if( mysqli_num_rows($result) > 0 ){
        while( $row = mysqli_fetch_assoc($result) ){
            $status = $row['status'];
        }
    }    
    
    if ($status == "Won")
    {
        echo "This auction cannot be deleted it has been won.";
    }
    else{
        $sql2 = 'DELETE FROM BID WHERE auctionID = '.$auctionID.';';
        
        if(mysqli_query($con, $sql2)) //&& mysqli_query($con, $sql2) )
        {
            echo "Related Bids have been deleted";
            echo '<br>';
            $sql3 = 'DELETE FROM AUCTION WHERE auctionID = '.$auctionID.';';
            if(mysqli_query($con, $sql3)) //&& mysqli_query($con, $sql2) )
            {
                echo "Auction has been deleted";
                echo '<br>';
            }
            else
            {
                echo "Unable to delete Auction.";
                echo '<br>';
            }  

        }
        else
        {
            echo "Unable to delete bids.";
            echo '<br>';
        }         
    }    
}

function updateAuctionQuery($con, $auctionID, $sellerID, $auctionStart, $auctionEnd, $reserve, $status, $title, $description, $winnerID, $bidAmount, $bidID, $buyerID, $bid)
{   
    if($bid)
    {
        $sql = 'UPDATE BID SET ';
        $sql .= 'bidAmount = "'.$bidAmount.'", ';
//        $sql .= 'buyerID = "'.$buyerID.'"';
        $sql .= 'WHERE bidID='.$bidID;
 
//        $sql2 = 'UPDATE AUCTION SET ';
//        $sql2 .= 'winnerID = "'.$winnerID.'" ';
//        $sql2 .= 'WHERE auctionID = "'.$auctionID.'" ';
 
        if(mysqli_query($con, $sql)) //&& mysqli_query($con, $sql2) )
        {
            echo "Your bid on: Auction #".$auctionID.": '".$title."' was successful";
        }
        else
        {
            echo "*Yoda Voice* That...is why you fail..";
        }       
    }
    else
    {
 
        $sql = 'UPDATE AUCTION SET ';
        $sql .= 'reserve='.$reserve.', ';
        $sql .= 'description="'.$description.'" ';
        $sql .= 'WHERE auctionID='.$auctionID.' ';
		echo $sql;
 
        if(mysqli_query($con, $sql) )
        {
            echo "Auction #".$auctionID.": '".$title."' was successfully updated.";
        }
        else
        {
            echo "*Yoda Voice* That...is why you fail..";
        }
    }       
}

function viewAuctions($con){
	$username = $_SESSION['id'];

    $sql = 'SELECT MAX(B.bidAmount) as CurrentBid, U.userName as BuyerName, TIMEDIFF(auctionEnd, now()) as TimeRemaining, A.auctionID, A.sellerID, A.auctionStart, A.auctionEnd, A.reserve, A.status, A.title, A.description, A.winnerID from BID as B RIGHT JOIN AUCTION as A on A.auctionID = B.auctionID LEFT JOIN BUYER AS BUY ON B.buyerID = BUY.buyerID LEFT JOIN USER as U on U.userName = BUY.userName WHERE A.status IN ("Active") GROUP BY A.auctionID;';
    //echo $sql;
    //TIMEDIFF(auctionEnd, now()) as TimeRemaining

	$result = mysqli_query($con, $sql);
	
	$markUp  = '<table>';
	$markUp .= '<tr>';
	$markUp .= '<th>Seller Name:</th>';
	$markUp .= '<th>Item Name: </th>';
	$markUp .= '<th>Item Description: </th>';
    $markUp .= '<th>Current High Bid: </th>';
    $markUp .= '<th>Time Remaining: </th>';
	$markUp .= '</tr>';
	if( mysqli_num_rows($result) > 0 ){
		while( $row = mysqli_fetch_assoc($result) ){
			$markUp .= '<tr>';
			$markUp .= '<td>'.getSellerName($con, $row['auctionID']).'</td>';
			$markUp .= '<td>'.$row['title'].'</td>';
			$markUp .= '<td>'.$row['description'].'</td>';
            $markUp .= '<td> $'.$row['CurrentBid'].'</td>';
            $markUp .= '<td> '.$row['TimeRemaining'].'</td>';
			$markUp .= '<td><a href="index.php?page=bid&buyerid='.$username.'&aid='.$row['auctionID'].'"><span class="tool">BID</span></a></td>';
			$markUp .= '</tr>';
		}

	}
	$markUp .= '</table>';

	return $markUp;
	

}

function bidForm($con, $itemName){
	$auctionID = $_GET['aid'];
    $currentBidValue = (int)getBidAmount($con, $auctionID);
    $minimumBid = $currentBidValue+1;


    $markUp  = '<h1>Bid on '.$itemName.'</h1>';
    $markUp .= '<div class="userFormBlock">';
    $markUp .= '<form class="siteForm" method="POST" > ';
    /*$markUp .= '<input type="hidden" name="auctionID" value="'.$auctionID.'"/>';
    $markUp .= '<input type="hidden" name="currentBid" value="'.$currentBidValue.'"/>';
    $markUp .= '<input type="hidden" name="minBid" value="'.$minimumBid.'"/>';*/
    $markUp .= '<label>Item Name: '.$itemName.'</label>';
    $markUp .= '<label>Current High Bid: $'.getBidAmount($con, $auctionID).'</label>';
    $markUp .= '<br /><br />';
    $markUp .= '<span class="dollarSign">$</span><input type="number" min="'.$minimumBid.'" value="'.$minimumBid.'"name="bidAmount" />';
    $markUp .= '<input type="submit" name="placeBid" value="Place Bid" />';
    $markUp .= '</form>';
    $markUp .= '</div>';

    $_SESSION['minBid'] = $minimumBid;
     
    echo $markUp;	

}

function getBidAmount($con, $auctionID){
	$sql  = 'SELECT MAX(bidAmount) as bidAmount ';
	$sql .= 'FROM BID ';
	$sql .= 'WHERE auctionID = '.$auctionID;

	$currentBid = '';

	$result = mysqli_query($con, $sql);
	if( mysqli_num_rows($result) > 0 ){
		while( $row = mysqli_fetch_assoc($result) ){
			$currentBid = $row['bidAmount'];
		}

	}
	if($currentBid == NULL){
		$currentBid = 0;
	}

	return $currentBid;

}

function getAuctionName($con, $auctionID){
    $sql  = 'SELECT title ';
    $sql .= 'FROM AUCTION ';
    $sql .= 'WHERE auctionID = '.$auctionID;

    $result = mysqli_query($con, $sql);
    if( mysqli_num_rows($result) > 0 ){
        while( $row = mysqli_fetch_assoc($result) ){
            $auctionName = $row['title'];
        }
    }

    return $auctionName;


}

function placeBid($con, $bidAmount, $auctionID, $buyerID){
    $sql2 = 'SELECT buyerID from BUYER WHERE userName = "'.$buyerID.'";';
    //echo $sql2;
    $result = mysqli_query($con, $sql2);
    if( mysqli_num_rows($result) > 0 ){
        while( $row = mysqli_fetch_assoc($result) ){
            $buyer = $row['buyerID'];
        }
    }

    $sql = 'INSERT INTO BID (bidID, buyerID, auctionID, bidTime, bidAmount) VALUES ((SELECT MAX(B.bidID)+1 FROM BID as B), "'.$buyer.'", "'.$auctionID.'", now(), '.$bidAmount.');';
    //echo $sql; 

    if( mysqli_query($con, $sql) ){
        header('Location: index.php?page=viewAuctions');

    }
    else{
        echo "Bid failed for some reason or another.";
    }
}

function createAuctionForm($con, $itemName = NULL, $sellerID = NULL, $auctionEnd = NULL, $reserve = NULL){
    $auctionDuration = array(6, 24, 48);
	
	$username = $_SESSION['id'];
    $date = date('Y/m/d H:i:s');

    $markUp  = '<h1>Create Auction</h1>';
    $markUp .= '<div class="userFormBlock">';
    $markUp .= '<form class="siteForm" method="POST" > ';
    $markUp .= '<label>Username: '.$username.'<br /><br /></label> ';
    $markUp .= '<label>Item Name:</label>';
    $markUp .= '<input type="text" name="itemName" value="'.$itemName.'" />';
    $markUp .= '<label>Start Date:</label>';
    $markUp .= '<input id="startDate" type="text" name="startDate" value="'.$date.'" />';
    $markUp .= '<label>Auction Duration in Days:</label>';
    $markUp .= '<input id="duration" type="text" name="duration" value="1" onChange="updateDate()" />';
    //$markUp .=  selectFormElement($auctionDuration, "duration", $auctionDuration[0], "Hours");
    //$markUp .= '<input type="text" class="datepicker" name="startDate" value="MM/DD/YYYY" />';
	//$markUp .= '<label>End Date:</label>';
	//$markUp .= '<input id="endDate" type="text" name="endDate" value="" />';
	$markUp .= '<label>Reserve:</label>';
	$markUp .= '<input type="number" name="reserve" min="1" value="'.$reserve.'"';
	
	$markUp .= '<label>Description:</label>';
    $markUp .= '<textarea name="itemDescription"></textarea>';


    $markUp .= '<br /><br />';
    $markUp .= '<input type="submit" name="createAuction" value="Create Auction" />';
    $markUp .= '</form>';
    $markUp .= '</div>';    

    return $markUp;

}

function insertAuction($con, $userName, $start, $endTime, $reser, $status = NULL, $item, $desc, $winnerID = NULL ){

    // Get the Seller ID
    $sql2 = 'SELECT sellerID from SELLER WHERE userName = "'.$userName.'";';
    
    $result = mysqli_query($con, $sql2);
    if( mysqli_num_rows($result) > 0 ){
        while( $row = mysqli_fetch_assoc($result) ){
            $seller = $row['sellerID'];
        }
    }

    // Get the current auctionID to use.
    $sql3 = 'SELECT MAX(auctionID)+1 as auctionID FROM AUCTION;';
    
    $result2 = mysqli_query($con, $sql3);
    if( mysqli_num_rows($result2) > 0 ){
        while( $row = mysqli_fetch_assoc($result2) ){
            $auctionID = $row['auctionID'];
        }
    }

    if( $reser == ''){
        $reser = '0';
    }

	if( $status == NULL ){
		$status = "Active";
	}
	
	if( $winnerID == NULL ){
		$winnerID = 'NULL';
	}
    
	
	$sql  = 'INSERT INTO AUCTION(auctionID, sellerID, auctionStart, auctionEnd, reserve, status, title, description, winnerID) ';
	$sql .= 'VALUES ( ';
	$sql .= $auctionID.', ';
	$sql .= $seller.', ';
	$sql .= '"'.$start.'", ';
	$sql .= '"'.$endTime.'", ';
	$sql .= $reser.', ';
	$sql .= '"'.$status.'", ';
	$sql .= '"'.$item.'", ';
	$sql .= '"'.$desc.'", ';
	$sql .= $winnerID;
	$sql .= ')';
	
	
    if( mysqli_query($con, $sql) ){
        header('Location: index.php?page=viewAuction');

    }
    else{
        echo "Unable to insert Auction";
    }
	
	//var_dump( mysqli_query($con, $sql) );



}


function showAuctionsNoAuth($con){
    $sql = 'SELECT MAX(B.bidAmount) as CurrentBid, U.userName as BuyerName, A.auctionID, A.sellerID, A.auctionStart, A.auctionEnd, A.reserve, A.status, A.title, A.description, A.winnerID from BID as B RIGHT JOIN AUCTION as A on A.auctionID = B.auctionID LEFT JOIN BUYER AS BUY ON B.buyerID = BUY.buyerID LEFT JOIN USER as U on U.userName = BUY.userName WHERE A.status IN ("Active") GROUP BY A.auctionID;';


    
    $result = mysqli_query($con, $sql);
    
    $markUp  = '<table>';
    $markUp .= '<tr>';
    $markUp .= '<th>Seller Name:</th>';
    $markUp .= '<th>Item Name: </th>';
    $markUp .= '<th>Item Description: </th>';
    $markUp .= '<th class="currentHighBid">Current High Bid: </th>';
    $markUp .= '</tr>';
    if( mysqli_num_rows($result) > 0 ){
        while( $row = mysqli_fetch_assoc($result) ){
            $markUp .= '<tr>';
            $markUp .= '<td>'.getSellerName($con, $row['auctionID']).'</td>';
            $markUp .= '<td>'.$row['title'].'</td>';
            $markUp .= '<td>'.$row['description'].'</td>';
            $markUp .= '<td class="currentHighBid"> $'.$row['CurrentBid'].'</td>';
            //$markUp .= '<td><a href="index.php?page=bid&buyerid='.$username.'&aid='.$row['auctionID'].'"><span class="tool">BID</span></a></td>';
            $markUp .= '</tr>';
        }

    }
    $markUp .= '</table>';

    return $markUp;
}





?>