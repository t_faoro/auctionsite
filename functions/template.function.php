<?php 
	$countrySel = array("CA", "US");
	$provinceSel = array("AB", "BC", "SK");
	$streetDirSel = array("N", "E", "S", "W");
	$endHour = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
    $endMinute = array("00", "15", "30", "45");
    $endAMPM = array("am", "pm");




function insertUser($con, $userName, $passwd, $country, $prov_state, $addr_unit, $addr_houseNum, $addr_street, $addr_city, $addr_streetDirection, $addr_postCode){

	$sql  = 'INSERT INTO USER (userName, passwd, country, prov_state, addr_unit, addr_houseNum, addr_street, addr_city, addr_streetDirection, addr_postCode ) ';
	$sql .= 'VALUES( ';
	$sql .= '"'.$userName.'", ';
	$sql .= '"'.$passwd.'", ';
	$sql .= '"CA", ';
	$sql .= '"'.$prov_state.'", ';
	$sql .= '"'.$addr_unit.'", ';
	$sql .= $addr_houseNum.', ';
	$sql .= '"'.$addr_street.'", ';	
	$sql .= '"'.$addr_city.'", ';	
	$sql .= '"N", ';	
	$sql .= '"'.$addr_postCode.'"';	
	$sql .= ' ) ';

	if(mysqli_query($con, $sql)){
		echo "User successfully added to Database.";
	}
	else{
		echo "The Query Failed";	
	}
		
}

function insertBuyer($con, $userName){

	$sql = 'INSERT INTO BUYER (buyerID, userName) VALUES ((SELECT MAX(B.buyerID)+1 from BUYER as B), "'.$userName.'");';
	//echo $sql;

	if(mysqli_query($con, $sql)){
		echo "User successfully added to Buyer.";
	}
	else{
		echo "The Query Failed";	
	}		
}

function insertSeller($con, $userName){

	$sql = 'INSERT INTO SELLER (sellerID, userName) VALUES ((SELECT MAX(S.sellerID)+1 from SELLER as S), "'.$userName.'");';
	//echo $sql;

	if(mysqli_query($con, $sql)){
		echo "User successfully added to Seller.";
	}
	else{
		echo "The Query Failed";	
	}		
}

function drawUserForm($userName = NULL, $passwd = NULL, $country = NULL, $prov_state = NULL, $addr_unit = NULL, $addr_houseNum = NULL, $addr_street = NULL, $addr_city = NULL, $addr_streetDirection = NULL, $addr_postCode = NULL, $user_Type = NULL){
	$country = array("CA", "US");
	$province = array("AB", "BC", "SK");
	$streetDir = array("N", "E", "S", "W");
	
	$markUp  = '<h1>Create User</h1>';
	$markUp .= '<div class="userFormBlock">';
	$markUp .= '<form class="siteForm" method="POST" > ';
	$markUp .= '<label>Username: </label> ';
	$markUp .= '<input type="text" value = "'.$userName.'" name="username" />';
	$markUp .= '<label>Password: </label>';
	$markUp .= '<input type="password" name="passOne" />';
	$markUp .= '<label>Retype Password:</label>';
	$markUp .= '<input type="password" name="passTwo" />';
	$markUp .= '<label>Country:</label>';
	$markUp .= selectFormElement($country, "country");
	$markUp .= '<br />';
	$markUp .= '<label>Province:</label>';
	$markUp .= selectFormElement($province, "province");	
	$markUp .= '<label>Unit #:</label>';
	$markUp .= '<input type="text" name="unitNum" value="'.$addr_unit.'" />';
	$markUp .= '<label>House #:</label>';
	$markUp .= '<input type="text" name="houseNum" value="'.$addr_houseNum.'" />';
	$markUp .= '<label>Street:</label>';
	$markUp .= '<input type="text" name="street" value="'.$addr_street.'" />';
	$markUp .= '<label>Direction:</label>';
	$markUp .= selectFormElement($streetDir, "streetDirection");	
	$markUp .= '<label>City:</label>';
	$markUp .= '<input type="text" name="city" value="'.$addr_city.'" />';	
	$markUp .= '<label>Postal Code:</label>';
	$markUp .= '<input type="text" name="postal" value="'.$addr_postal.'" />';
	$markUp .= '<label>Account Type:</label>';
	$markUp .= '<input type="radio" name="accountType" value="Buyer" checked="checked" />Buyer<br> ';
	$markUp .= '<input type="radio" name="accountType" value="Seller" />Seller<br> ';
	$markUp .= '<br /><br />';
	$markUp .= '<input type="submit" name="insertUser" value="Insert" />';
	$markUp .= '</form>';
	$markUp .= '</div>';
	
	echo $markUp;
}

function drawUpdateUserForm($userName = NULL, $passwd = NULL, $country = NULL, $prov_state = NULL, $addr_unit = NULL, $addr_houseNum = NULL, $addr_street = NULL, $addr_city = NULL, $addr_streetDirection = NULL, $addr_postCode = NULL){
	global $provinceSel;
	global $countrySel;
	global $streetDirSel;

	$markUp  = '<h1>Update User</h1>';
	$markUp .= '<div class="userFormBlock">';
	$markUp .= '<form class="siteForm" method="POST" > ';
	$markUp .= '<label>Username: '.$userName.'<br /><br /></label> ';
	$markUp .= '<br /><br />';
	$markUp .= '<label>Password: </label>';
	$markUp .= '<input type="password" name="passOne" />';
	$markUp .= '<label>Retype Password:</label>';
	$markUp .= '<input type="password" name="passTwo" />';
	$markUp .= '<label>Country:</label>';
	$markUp .= selectFormElement($countrySel, "country", $country);
	$markUp .= '<br />';
	$markUp .= '<label>Province:</label>';
	$markUp .= selectFormElement($provinceSel, "province", $prov_state);	
	$markUp .= '<label>Unit #:</label>';
	$markUp .= '<input type="text" name="unitNum" value="'.$addr_unit.'" />';
	$markUp .= '<label>House #:</label>';
	$markUp .= '<input type="text" name="houseNum" value="'.$addr_houseNum.'" />';
	$markUp .= '<label>Street:</label>';
	$markUp .= '<input type="text" name="street" value="'.$addr_street.'" />';
	$markUp .= '<label>Direction:</label>';
	$markUp .= selectFormElement($streetDirSel, "streetDirection");	
	$markUp .= '<label>City:</label>';
	$markUp .= '<input type="text" name="city" value="'.$addr_city.'" />';	
	$markUp .= '<label>Postal Code:</label>';
	$markUp .= '<input type="text" name="postal" value="'.$addr_postCode.'"';

	$markUp .= '<br /><br />';
	$markUp .= '<input type="submit" name="updateUser" value="Update User" />';
	$markUp .= '</form>';
	$markUp .= '</div>';
	
	echo $markUp;
}

function getbuyerOrsellerID($con, $userName){
	$userType = isUserOrSeller($con, $userName);
	if ($userType == "Buyer"){
		$sql = 'SELECT buyerID from BUYER WHERE userName = "'.$userName.'";';
		$result = mysqli_query($con, $sql);
		if( mysqli_num_rows($result) > 0 ){
			while( $row = mysqli_fetch_assoc($result) ){
				$ID = $row['buyerID'];
			}
		}	
	}
	else{
		$sql2 = 'SELECT sellerID from SELLER WHERE userName = "'.$userName.'";';
		$result2 = mysqli_query($con, $sql2);
		if( mysqli_num_rows($result2) > 0 ){
			while( $row = mysqli_fetch_assoc($result2) ){
				$ID = $row['sellerID'];
			}
		}
	}

	return $ID;
}

function selectFormElement( $options, $name, $currVal = NULL){
	$markUp = '<select name="'.$name.'"> ';
	if($currVal == NULL){	
		foreach($options as $value){
			$markUp .= '<option>'.$value.'</option>';	
		}
	}
	else{
		
		foreach($options as $value){
			if($currVal == $value){
				$markUp .= '<option selected = "selected" >'.$value.'</option>';
			}
			else{
				$markUp .= '<option>'.$value.'</option>';			
			}	
		}		
	}
	$markUp .= '</select>';
	
	return $markUp;

}

function getUsernames($con){
	$sql  = 'SELECT userName ';
	$sql .= 'FROM USER ';
	
	$result = mysqli_query($con, $sql);
	$markUp = '<div class=">';

	if( mysqli_num_rows($result) > 0){
		//$markUp .= '<tr>';
		while($row = mysqli_fetch_assoc($result) ){
			$markUp .= '<tr><td>'.$row['userName'].'</td></tr>';
		}
		//$markUp .= '</tr>';
	}
	$markUp .= '</table>';
	
	echo $markUp;
}

function getUserUpdateData($con, $userName){
	$sql  = 'SELECT * ';
	$sql .= 'FROM USER ';
	$sql .= 'WHERE userName = "'.$userName.'" ';

	$result = mysqli_query($con, $sql);
	//var_dump($result);
	

	if( mysqli_num_rows($result) > 0){
		while($row = mysqli_fetch_assoc($result) ){
			$_SESSION['uname'] = $row['userName'];
			$_SESSION['pass'] = $row['passwd'];
			$_SESSION['country'] = $row['country'];
			$_SESSION['prov'] = $row['prov_state'];
			$_SESSION['unit'] = $row['addr_unit'];
			$_SESSION['houseNum'] = $row['addr_houseNum'];
			$_SESSION['street'] = $row['addr_street'];
			$_SESSION['city'] = $row['addr_city'];
			$_SESSION['direction'] = $row['addr_streetDirection'];
			$_SESSION['postal'] = $row['addr_postCode'];
		}
	}
	
}

function updateUserQuery($con, $userName, $passwd, $country, $prov_state, $addr_unit, $addr_houseNum, $addr_street, $addr_city, $addr_streetDirection, $addr_postCode){
	$sql  = 'UPDATE USER ';
	$sql .= 'SET ';
	if( $_POST['passOne'] == NULL && $_POST['passTwo'] == NULL ){	
		$sql .= 'passwd = "'.$_SESSION['pass'].'", ';
	}
	else{
		if( $_POST['passOne'] === $_POST['passTwo'] ){
			$sql .= 'passwd = "'.$_POST['passTwo'].'", ';		
		}
		else{
			echo "Password doesn't match";		
		}
	}
	$sql .= 'country = "'.$country.'", ';
	$sql .= 'prov_state = "'.$prov_state.'", ';
	$sql .= 'addr_unit = "'.$addr_unit.'", ';
	$sql .= 'addr_houseNum = '.$addr_houseNum.', ';
	$sql .= 'addr_street = "'.$addr_street.'", ';
	$sql .= 'addr_city = "'.$addr_city.'", ';
	$sql .= 'addr_streetDirection = "'.$addr_streetDirection.'", ';
	$sql .= 'addr_postCode = "'.$addr_postCode.'" ';
	$sql .= 'WHERE userName = "'.$userName.'" ';
	

	if(mysqli_query($con, $sql) ){
		echo $userName." was successfully updated.";	
	}
	else{
		echo "*Yoda Voice* That...is why you fail..";
	}

}

function deleteUserQuery($con, $userName){
	
	$userName = mysqli_real_escape_string($con, $userName);
	$userType;

	$sql  = 'SELECT * FROM BUYER WHERE userName = "'.$userName.'";';
	//echo $sql;
	$result = mysqli_query($con, $sql);
	
	if( mysqli_num_rows($result) == 0 ){
		$userType = "Seller";
		$sql7 = 'SELECT sellerID FROM SELLER WHERE userName = "'.$userName.'";';
		$result6 = mysqli_query($con, $sql7);
		if( mysqli_num_rows($result6) > 0 ){
			while( $row = mysqli_fetch_assoc($result6) ){
				$sellerID = $row['sellerID'];
				//echo $sellerID;
			}
		}

		// Do not allow a Seller who has a "Won" auction to delete their account.
		$sql8 = 'SELECT * FROM AUCTION WHERE sellerID = '.$sellerID.' and status = "Won";';
		//echo $sql8;
		$result7 = mysqli_query($con, $sql8);
		if( mysqli_num_rows($result7) > 0 ){

		}
		else
		{
			$sql9 = 'DELETE FROM BID WHERE auctionID IN (SELECT auctionID FROM AUCTION A WHERE A.sellerID = '.$sellerID.');';
			echo $sql9;
			echo '<br>';			
			$sql10 = 'DELETE FROM AUCTION WHERE sellerID = '.$sellerID.';';
			echo $sql10; 
			echo '<br>';	
			$sql11 = 'DELETE FROM SELLER WHERE sellerID = '.$sellerID.';';
			echo $sql11; 
			echo '<br>';	
			$sql12 = 'DELETE FROM USER WHERE userName = "'.$userName.'";';
			echo $sql12; 
			echo '<br>';	

			if(mysqli_query($con, $sql9) and mysqli_query($con, $sql10) and mysqli_query($con, $sql11) and mysqli_query($con, $sql12)){
				echo $userName.' was successfully deleted.';
				logout();
				header('Location: index.php');

			}
			else{
				echo " Delete User failed";
			}
		}

	}
	else{
		$userType = "Buyer";
		$sql3 = 'SELECT buyerID FROM BUYER WHERE userName = "'.$userName.'";';
		$result3 = mysqli_query($con, $sql3);
		if( mysqli_num_rows($result3) > 0 ){
			while( $row = mysqli_fetch_assoc($result3) ){
				$buyerID = $row['buyerID'];
				//echo $buyerID;
			}
		}

		// Do not allow a Buyer to Delete account if they have a currently "Won Auction"
		$sql2 = 'SELECT * FROM AUCTION WHERE winnerID = "'.$buyerID.'";';
		//echo $sql2;
		$result = mysqli_query($con, $sql2);
	
		if( mysqli_num_rows($result) > 0 ){
			echo '<br>';			
			echo "Buyer cannot be deleted you have Won an Auction";
		}
		else{
			echo '<br>';
			//echo "Buyer Can be deleted";
			$sql4 = 'DELETE FROM BID WHERE buyerID = "'.$buyerID.'";';	
			$sql5 = 'DELETE FROM BUYER WHERE buyerID = "'.$buyerID.'";';
			$sql6 = 'DELETE FROM USER WHERE userName = "'.$userName.'";';
						
			if(mysqli_query($con, $sql4) and mysqli_query($con, $sql5) and mysqli_query($con, $sql6)){
				echo $userName.' was successfully deleted.';
				logout();
				header('Location: index.php');
			}
			else{
				echo " Delete Bid failed";
			}				
		}
	}


}

function drawUserDeletePage($userName){
	$markUp  = '<h1>Delete User</h1>';
	$markUp .= '<div class="userFormBlock">';
	$markUp .= '<form class="siteForm" method="POST" > ';
	$markUp .= '<label name="userName">Username: </label> ';
	$markUp .= '<input type="text" name="userName" value="'.$userName.'" readonly />';
	$markUp .= '<br /><br />';
	
	$markUp .= '<label>Are you sure you want to delete: '.$userName.'?</label>';
	$markUp .= '<br /><br />';
	$markUp .= '<input type="submit" name="deleteUser" value="Delete '.$userName.'" />';
	$markUp .= '</form>';
	$markUp .= '</div>';
	
	echo $markUp;

}

function drawUserPage($con){
	$sql  = 'SELECT * ';
	$sql .= 'FROM USER ';
	
	$result = mysqli_query($con, $sql);
	//var_dump($result);
	$markUp  = '<a href="index.php?page=createUser"><span class="tool">CREATE</span></a>';
	$markUp  .= '<div class="dataPresent">';
		if( mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result) ){
				$markUp .= '<div class="dataCell"><span class="data">'.$row['userName'].'</span></div>';
				$markUp .=  getUserToolbox($row['userName'], "USER");
			}
		}
	$markUp .= '</div>';
	
	return $markUp;
	

}

function drawViewAuctions($con, $auctionTitle = NULL){	
	$auctionList = getListofAuctions($con);
	
    $markUp  = '<h1>View Auctions</h1>';
    $markUp .= '<div class="userFormBlock">';
    $markUp .= '<form class="siteForm" method="POST" > ';
    $markUp .= '<label>Select an Auction: '.$title.'</label> ';
    $markUp .= selectFormElement($auctionList, "Auction List");
    $markUp .= '<input type="submit" name="selectAuction" value="Select Auction" />';
    $markUp .= '</form>';
    $markUp .= '</div>';	

	
	return $markUp;
}

function getListofAuctions($con){
	$sql = 'SELECT title FROM AUCTION;';
	$result = mysqli_query($con, $sql);
	if( mysqli_num_rows($result) > 0){
		$columnValues = array();
		while ( $row = mysqli_fetch_assoc($result) ) {
			$columnValues[] = $row['title'];
		}
	return $columnValues;
	}
}

function getAuctionDetails($con, $auctionDescription){
	$sql = 'SELECT title FROM AUCTION;';
	$result = mysqli_query($con, $sql);
	if( mysqli_num_rows($result) > 0){
		$columnValues = array();
		while ( $row = mysqli_fetch_assoc($result) ) {
			$columnValues[] = $row['title'];
		}
	return $columnValues;
	}
}

//::=====> AUCTION LOGIC
//::===> SELLER
function drawSellerHome($con, $userID){
	$sql  = 'SELECT A.auctionID, A.title, S.userName from AUCTION AS A ';
	$sql .= 'LEFT JOIN SELLER AS S ON S.sellerID = A.sellerID ';
	$sql .= 'LEFT JOIN BID AS B ON A.winnerID = B.buyerID ';
	$sql .= 'WHERE A.status = "Active" '; 
	$sql .= 'AND A.sellerID = "'.$userID.'" GROUP BY A.auctionID;';
	//echo $sql;
	$result = mysqli_query($con, $sql);
	
	if( mysqli_num_rows($result) > 0){
		$markUp  .= '<table class="table">';
		$markUp .= '<tr>';
		$markUp .= '<th colspan="3" class="left">Current Active Auctions:</th>';
		$markUp .= '<td>&nbsp;</td>';
		$markUp .= '<td>&nbsp;</td>';
				
		$markUp .= '</tr>';
		while( $row = mysqli_fetch_assoc($result) ){
			$markUp .= '<tr class="auctionRow">';
			$markUp .= '<td class="activeAuctionSeller">'.$row['title'].'</td>';
			
		}
		$markUp .= '</table>';
		$markUp .= '<hr />';		
	}

	$sql2 = 'SELECT A.title, B.userName as Winner from AUCTION as A LEFT JOIN BUYER as B on A.winnerID = B.buyerID WHERE A.status = "Won" ';
	$sql2 .= 'AND A.sellerID = "'.$userID.'";';

	$result2 = mysqli_query($con, $sql2);
	
	if( mysqli_num_rows($result2) > 0){
		$markUp  .= '<table class="table">';
		$markUp .= '<tr>';
		$markUp .= '<th colspan="3" class="left">Auctions Which Have Been Won:</th>';
		$markUp .= '<td>&nbsp;</td>';
		$markUp .= '<td>&nbsp;</td>';
				
		$markUp .= '</tr>';
		while( $row = mysqli_fetch_assoc($result2) ){
			$markUp .= '<tr class="auctionRow">';
			$markUp .= '<td class="activeAuctionSeller">'.$row['title'].' - Has been Won by: '.$row['Winner'].'</td>';			
		}
		$markUp .= '</table>';
		$markUp .= '<hr />';		
	}

	return $markUp;

}
//::::======> BUYER
function drawBuyerHome($con, $userID){
	$sql = 'SELECT A.auctionID,  A.title, S.userName from AUCTION AS A LEFT JOIN SELLER AS S ON S.sellerID = A.sellerID LEFT JOIN BID AS B ON A.winnerID = B.buyerID WHERE A.status = "WON" and A.winnerID = "'.$userID.'" GROUP BY A.auctionID;';
	//echo $sql;

	$result = mysqli_query($con, $sql);
	
	if( mysqli_num_rows($result) > 0){
		$markUp  = '<h2>Congratulations! You have won the following:</h2>';
		$markUp  .= '<table class="table">';
		$markUp .= '<tr>';
		$markUp .= '<th colspan="3" class="left">Title</th>';
		$markUp .= '<td>&nbsp;</td>';
		$markUp .= '<td>&nbsp;</td>';
		$markUp .= '<th>Seller</th>';
		
		$markUp .= '</tr>';
		while( $row = mysqli_fetch_assoc($result) ){
			$markUp .= '<tr class="auctionRow">';
			$markUp .= '<td colspan="3">'.$row['title'].'</td>';
			$markUp .= '<td>&nbsp;</td>';
			$markUp .= '<td>&nbsp;</td>';
			$markUp .= '<td colspan="3">'.$row['userName'].'</td>';
			$markUp .= '<td>&nbsp;</td>';
			$markUp .= '<td>&nbsp;</td>';
			$markUp .= '</table>';
		}
		return $markUp;
	}

}

function drawAuctions($con){
	$username = $_SESSION['id'];
	$sql2 = 'SELECT sellerID FROM SELLER WHERE userName = "'.$username.'";';

	$result2 = mysqli_query($con, $sql2);
	if( mysqli_num_rows($result2) > 0){		
		while ( $row = mysqli_fetch_assoc($result2) ) {
			$seller = $row['sellerID'];		
		}	
	}
	// @TODO Check this area. May allow circumvension of login system.
	$sql5 = 'SELECT * FROM AUCTION WHERE sellerID = "'.$seller.'";';
	$result5 = mysqli_query($con, $sql5);
	if ( mysqli_num_rows($result5) == 0 ){
		$markUp = 'You currently have no Auctions';
		$markUp .= '<a href="index.php?page=createAuction&uname='.$_SESSION['id'].'"><span class="tool">CREATE</span></a>';
	}
	else{

	    $sql = 'SELECT MAX(B.bidAmount) as CurrentBid, TIMEDIFF(auctionEnd, now()) as TimeRemaining, A.auctionID, A.sellerID, A.auctionStart, A.auctionEnd, A.reserve, A.status, A.title, A.description, A.winnerID from AUCTION A LEFT JOIN BID B ON A.auctionID = B.auctionID WHERE A.sellerID = "'.$seller.'"AND A.status NOT IN ("Cancelled") GROUP BY A.auctionID;';

	    //echo $sql;
	    //echo '<br>';
		$result = mysqli_query($con, $sql);
		if( mysqli_num_rows($result) > 0){
			$markUp  = '<table class="auctionData">';
			$markUp .= '<tr>';
			$markUp .= '<th>Seller</th>';
			$markUp .= '<th>Title</th>';
			$markUp .= '<th>Max Bid</th>';
			$markUp .= '<th>Time Left</th>';
			$markUp .= '<th>Auction Status</th>';
			$markUp .= '</tr>';
			
			//$markUp  = '<div class="dataPresent">';
			$markUp .= '<a href="index.php?page=createAuction&uname='.$_SESSION['id'].'"><span class="tool">CREATE</span></a>';

			while( $row = mysqli_fetch_assoc($result) ){

				$CheckOver = $row['auctionEnd'];
				$CheckNow = date('Y-m-d H:i:s');
				$AuctionID = $row['auctionID'];
				


				if($CheckNow > $CheckOver)
				{

					$sql2 = 'SELECT MAX(B.bidAmount) as MaxBid, B.buyerID, A.reserve, A.auctionID FROM AUCTION as A INNER JOIN BID as B on A.auctionID = B.auctionID ';
					$sql2 .= 'WHERE A.auctionID = ' .$AuctionID;

					//echo $sql2;
					//echo '<br>';
					$result2 = mysqli_query($con, $sql2);

					while( $row2 = mysqli_fetch_assoc($result2) ){
						$MaxBid = $row2['MaxBid'];
						$Reserve = $row2['reserve'];
						$HighBidID = $row2['buyerID]'];

						if($MaxBid > $Reserve)
						{
							//echo "Auction Has been won do something";
							$sql3 = 'UPDATE AUCTION SET status = "Won", winnerID = ' .$row2['buyerID'] ;
							$sql3 .= ' WHERE auctionID = ' .$AuctionID;

							if(mysqli_query($con, $sql3)){
								//echo "Auction Updated to Win";
							}	
						}
						else
						{
							//echo "Auction reserve has not been met";
							$sql4 = 'UPDATE AUCTION SET status = "Cancelled" ' ;
							$sql4 .= ' WHERE auctionID = ' .$AuctionID;

							if(mysqli_query($con, $sql4)){
								//echo "Auction Reserve Not Met Auction Cancelled";
							}							
						}
					}

					//echo "Auction Over";
				}	

				$markUp .= '<tr class="auctionRow">';
				$markUp .= '<td>'.getSellerName($con, $row['auctionID'] ).'</td>';
				$markUp .= '<td>'.$row['title'].'</td>';
				$markUp .= '<td>$'.$row['CurrentBid'].'</td>';
				 //Removes the -365:blah:blah glitch when the time runs out.
                if($row['status'] == "Won")
                    $markUp .= '<td>Finished</td>';   
                else
                    $markUp .= '<td>'.$row['TimeRemaining'].'</td>';
				$markUp .= '<td>'.$row['status'].'</td>';
				if( $_SESSION['authenticated'] ){
					$markUp .=  getAuctionToolBox($row['auctionID'], $_SESSION['id'] );
				}
			}
		}
		//$markUp .= '</div>';
		$markUp .= '</table>';
	}
	return $markUp;
	

}

function getActiveSellerAuctions($con){
	
	$username = $_SESSION['id'];
	
	$sql  = 'SELECT * ';
	$sql .= 'FROM AUCTION A, SELLER S ';
	$sql .= 'WHERE S.userName = "'.$username.'" ';
	$sql .= 'AND A.sellerID = S.sellerID ';
	//echo $sql;
	$markUp = '<h1>'.$username.'\'s Active Auctions</h1>';

	$markUp .= '<table>';
	$markUp .= '<th>Auction ID</th>';
	$markUp .= '<th>Title:</th>';
	$markUp .= '<th>Description:</th>';
	$markUp .= '<th>&nbsp;</th>';
	$result = mysqli_query($con, $sql);
	if( mysqli_num_rows($result) > 0 ){
		while( $row = mysqli_fetch_assoc($result) ){
			$markUp .= '<tr>';
			$markUp .= '<td>'.$row['auctionID'].'</td>';
			$markUp .= '<td>'.$row['title'].'</td>';
			$markUp .= '<td>'.$row['description'].'</td>';
			$markUp .= '<td><a href="index.php?page=updateAuction&id='.$username.'"><span class="tool">UPDATE</span></a></td>';
			$markUp .= '</tr>';
		}

	}
	$markUp .= '</table>';

	return $markUp;

}

function getActiveBids($con, $username){

}

function returnWinningBidder($auctionID, $username){


}


//::>> Start Eric's Block

function getAuctionUpdateData($con, $auctionID)
{
     
    //Grab required info from Auction

 
    $sql  = 'SELECT MAX(B.bidAmount), A.auctionID, A.sellerID, A.auctionStart, A.auctionEnd, A.reserve, A.status, A.title, A.description, A.winnerID, B.bidID, B.buyerID FROM AUCTION AS A INNER JOIN BID AS B on A.auctionID = B.auctionID WHERE ';
    $sql .= 'A.auctionID = '.$auctionID.';';
    $result = mysqli_query($con, $sql);
    //var_dump($result);
     
 
    if( mysqli_num_rows($result) > 0)
    {
        while($row = mysqli_fetch_assoc($result) )
        {
            $_SESSION['auctionID']        = $row['auctionID'];
            $_SESSION['sellerID']        = $row['sellerID'];
            $_SESSION['auctionStart']    = $row['auctionStart'];
            $_SESSION['auctionEnd']        = $row['auctionEnd'];
            $_SESSION['reserve']        = $row['reserve'];
            $_SESSION['status']        = $row['status'];
            $_SESSION['title']        = $row['title'];
            $_SESSION['description']    = $row['description'];
            $_SESSION['winnerID']        = $row['winnerID'];
            $_SESSION['bidAmount']        = $row['bidAmount'];
            $_SESSION['bidID']        = $row['bidID'];
            $_SESSION['buyerID']        = $row['buyerID'];
             
        }
    }
     
}
 
function drawAuctionDeletePage($con, $auctionID){
	$sql  = 'SELECT title FROM AUCTION ';
	$sql .= 'WHERE auctionID = '.$auctionID;
	//echo $sql;

	$result = mysqli_query($con, $sql);
	if( mysqli_num_rows($result) > 0 ){
		while( $row = mysqli_fetch_assoc($result) ){
			$auctionTitle = $row['title'];
		}
	}

	$markUp  = '<h1>Delete Auction</h1>';
	$markUp .= '<div class="userFormBlock">';
	$markUp .= '<form class="siteForm" method="POST" > ';
	$markUp .= '<label>Are you sure you want to delete: '.$auctionTitle.'?</label>';
	$markUp .= '<br /><br />';
	$markUp .= '<input type="submit" name="deleteAuction" value="Delete" />';
	$markUp .= '</form>';
	$markUp .= '</div>';
	
	echo $markUp;

}

function drawUpdateAuctionForm($con, $auctionID = NULL, $sellerID = NULL, $auctionStart = NULL, $auctionEnd = NULL, $reserve = NULL, $status = NULL, $title = NULL, $description = NULL, $winnerID = NULL, $bidAmount = NULL, $bidID = NULL, $buyerID = NULL)
{
    //global $endHour;
    global $endMinute;
    global $endAMPM;
 


    $markUp  = '<h1>Update Auction</h1>';
    $markUp .= '<div class="userFormBlock">';
    $markUp .= '<form class="siteForm" method="POST" > ';
    $markUp .= '<label>Auction Name: '.$title.'<br /><br /></label> ';
    $markUp .= '<br /><br />';
    $markUp .= '<label>Item description: </label>';
    $markUp .= '<textarea style="resize:none; height:100px; width:100%" name="description" >'.$description.'</textarea>';
    $markUp .= '<br />';
    $markUp .= '<label>Reserve:</label>';
    $markUp .= '<input type="text" name="reserve" value="'.$reserve.'" />';
    $markUp .= '<br /><br />';
    $markUp .= '<input type="submit" name="updateAuction" value="Update Auction" />';
    $markUp .= '</form>';
    $markUp .= '</div>';
     
    echo $markUp;
}

function selectElementDate( $options, $name, $currVal = NULL){
    $sql  = 'SELECT YEAR(auctionEnd) as Year, MONTH(auctionEnd) as Month, DAY(auctionEnd) as Day, HOUR(auctionEnd) as Hour, MINUTE(auctionEnd) as Minute, SECOND(auctionEND) as Second from AUCTION where auctionID = ' .$auctionID;

	$result = mysqli_query($con, $sql);
	
	$markUp  = '<select>';
	if(mysqli_num_rows($result) > 0){
		while($row = mysqli_fetch_assoc($result) ){
			$markUp .= '<option>'.$row['Hour'].'</option>';
		}

	}
	$markUp .= '</select>';

    $markUp = '<select style="width:14%" name="'.$name.'"> ';
    if($currVal == NULL){     
        foreach($options as $value){
            $markUp .= '<option>'.$value.'</option>';     
        }
    }
    else{
         
        foreach($options as $value){
            if($currVal == $value){
                $markUp .= '<option selected = "selected" >'.$value.'</option>';
            }
            else{
                $markUp .= '<option>'.$value.'</option>';             
            }     
        }         
    }
    $markUp .= '</select>';
     
    return $markUp;
 
}
 

//::==> END ERIC's BLOCk

//::==> START MATT'S BLOCK

function isUserOrSeller($con, $userName){

    $sql = 'SELECT * FROM BUYER as B INNER JOIN USER AS U ON B.userName = U.userName';
    $sql .= ' WHERE B.userName = "'.$userName.'" ';

    $result = mysqli_query($con, $sql);

    if( mysqli_num_rows($result) > 0){
        $markUp = 'Buyer';       
        return $markUp;
    }
   
    $sql = 'SELECT * FROM SELLER as S INNER JOIN USER AS U ON S.userName = U.userName';
    $sql .= ' WHERE S.userName = "'.$userName.'" ';
   
    $result = mysqli_query($con, $sql);
    if( mysqli_num_rows($result) > 0){
        $markUp = 'Seller';
        return $markUp;
    }
}


//::==> END MATT's BLOCK


















?>
