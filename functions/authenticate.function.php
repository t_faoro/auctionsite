<?php 
session_start();
function drawLogin(){
	$currentPage = basename($_SERVER['PHP_SELF']);	
			
	$markUp  = '<div id="loginContainer" >';
		
	$markUp .= '<div id="loginFormHorizontal" >';
	$markUp .= '<form class="login" method="POST">';
	$markUp .= '<label for="user">Username: </label>';
	$markUp .= '<input type="text" name="user" /><br />';
	$markUp .= '<label for="pass">Password:</label>';
	$markUp .= '<input type="password" name="pass" /><br /><br />';
	$markUp .= '<input type="submit" name="login" value="Login" />';
	$markUp .= '</form>';
	$markUp .= '</div>';
		
	$markUp .= '</div>';
		
		
	return $markUp;
		
}

function drawInlineLogin($error = NULL){
	$currentPage = basename($_SERVER['PHP_SELF']);	
			
	$markUp  = '<div id="loginContainer" >';
		
	$markUp .= '<div id="loginForm" >';
	$markUp .= '<form class="login" method="POST">';
	$markUp .= '<label for="user">Username: </label>';
	$markUp .= '<input type="text" name="user" />';
	$markUp .= '<label for="pass">Password:</label>';
	$markUp .= '<input type="password" name="pass" />';
	$markUp .= '<input type="submit" name="login" value="Login" />';
	//$markUp .= $error;
	$markUp .= '</form>';
	
	$markUp .= '</div>';
		
	$markUp .= '</div>';
		
		
	return $markUp;
		
}

function checkAuth($con){
	if( isset($_POST['login']) ){
		
		$username = $_POST['user'];
		$password = $_POST['pass'];
		
		checkLogin($con, $username, $password);
	}	
	else{
		echo drawInlineLogin();
	}

}

function authUser() {
	
	if(  isset($_SESSION['authenticated']) && isset($_SESSION['id']) ){ 
		return true;
	}
	else{
		 return false;
			 
	}
}

function checkLogin($con, $username, $password){				
		$sql  = 'SELECT * ';
		$sql .= 'FROM USER ';
		$sql .= 'WHERE userName = "'.$username.'" ' ;
		$sql .= ' AND ';
		$sql .= 'passwd = "'.$password.'" ';
	
		$query 	= mysqli_query($con, $sql);
		$row	= mysqli_num_rows( $query );
		
		//var_dump($query);
		//:: Login Failed
		if($row <= 0 || $row > 1){
			$error = '<span class="error">Login Failed</span>';
			$_SESSION['authenticated'] = FALSE;
			echo drawInlineLogin($error);
			
		}
		//:: Login Successful
		if($row == 1){				
			sessionSetter($query);				
			header('location: index.php');
				
		}		
		
		//:: This else is very simply for an unforseen circumstance, nothing more.
		/*else{
			$_SESSION['authenticated'] = FALSE;
		}*/
				
}

function sessionSetter($query){
		$_SESSION['authenticated'] = TRUE;
	while($row = mysqli_fetch_assoc($query)){
		
		$_SESSION['id'] = $row['userName'];	
	}		
	
}

function logout(){
		//$_SESSION['authenticated'] = FALSE;
		unset($_SESSION['authenticated']);
		unset($_SESSION['id']);
		session_unset();
		header('location: index.php');	
}

function drawLogout(){
	$markUp  = '<div class="logoutArea">';
	$markUp .= '<span><p>Hello  '.$_SESSION['id'].'</span>';
	$markUp .= ' Click <a href="index.php?action=logout">here</a> to logout.</p>';
	$markUp .= '</div>';
	
	return $markUp;
}




?>