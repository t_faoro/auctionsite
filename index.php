<!doctype html>
<?php 
session_start();
include_once "config.php"; 

$con = new Connection();
$con = $con->connect();
	if (mysqli_errno($con)){
		echo "Connection to Database Failed.";
		die();
	}
/**
* AUTHENTICATION SESSSIONS TO BE USED BY SITE.
* $_SESSION['authenticated'] 
* ==> IF TRUE: We're logged in
* ==> ELSE: We're not Logged in 
*/
/*
//::::::THIS LINE MUST BE CHANGED BEFORE SITE IS FINALIZED:::::::::\\
/*******Current Set be statically true until finished*******/
/**/		//$_SESSION['authenticated'] = FALSE;			/**/
/**********************************************************/
//$_SESSION['id'] = NULL;



?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
<meta charset="utf-8">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    
<!-- Load jQuery JS -->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<!-- Load jQuery UI Main JS  -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    
<!-- Load SCRIPT.JS which will create datepicker for input field  -->
<script src="js/script.js"></script>

<title>BidHouse International - Pants optional</title>
</head>

<body>
	<!-- Main Layout -->
    <div  id="container">
    	<!-- Header Layout -->
                
        <div id="headerContainer">
        <div id="loginLocation">
			<?php	
				/**
				* Authentication Block
				*    -- Everything between these PHP delimeters will handle the login action. 
				* 		Will output a Logout area with respective link if logged in, and a login
				*		box if not logged in.
				*/
				
				//::===> Authentication START
				if($_SESSION['authenticated'] == TRUE){
					echo drawLogout();				
				}
				else{
					if(isset($_POST['login']) ){
						$username = $_POST['user'];
						$password = $_POST['pass'];
						
						$sql  = 'SELECT * ';
						$sql .= 'FROM USER ';
						$sql .= 'WHERE userName = "'.$username.'" ' ;
						$sql .= ' AND ';
						$sql .= 'passwd = "'.$password.'" ';
					
						$query 	= mysqli_query($con, $sql);
						$row = mysqli_num_rows( $query );
												
						if($row <= 0 || $row > 1){
							$error = '<span class="error">Login Failed</span>';
							$_SESSION['authenticated'] = NULL;
							echo "Login Failed";
							echo drawInlineLogin();

							
						}
						//:: Login Successful
						if($row == 1){				
							while($row = mysqli_fetch_assoc($query)){
								$_SESSION['authenticated'] = TRUE;
								$_SESSION['id'] = $row['userName'];
								echo drawLogout();

							}
							header('Location: index.php');							
						}		
						
						//:: This else is very simply for an unforseen circumstance, nothing more.
						/*else{
							$_SESSION['authenticated'] = FALSE;
						}*/
					}
					else{
						echo drawInlineLogin();
					}
				}
				//::==> Authentication END				
		//var_dump($_SESSION);
			?>
        </div>
        	<div id="headerContent">&nbsp;</div>
        </div>
        
        <!-- Content Layout -->
            <div id="content">
                <div id="side">
                	<?php                   
	                    if($_SESSION['authenticated'] == FALSE )
	                    {
	                    	
	                         drawNav();
	                    }                   
	                    else //if($_SESSION['authenticated'] == TRUE )
	                    {
	                    	$username = $_SESSION['id'];
	                    	$userType = isUserOrSeller($con, $username);
	                        if($userType == 'Seller'){
	                            
	                            drawNavSeller();
	                            
	                        }
	                        elseif($userType == 'Buyer'){
	                            drawNavBuyer();
	                            
	                        }
	                    }
                    ?>
                </div>
                <div id="main">
                
                	<?php 
						/**
						*	This is where the page switch will be included to display our content.
						*	Note: See /page/page.switch.php to edit this.
						*/
						include_once "page/page.switch.php" 
						
					?>
                    
                </div>            
            </div>
        
        
        <!-- Footer Layout -->
        <div id="footerContainer">
        	<div id="footerContent">&nbsp;</div>
        </div>    
    
    </div> <!-- end of container -->


</body>
</html>

