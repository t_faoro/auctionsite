<?php 
/** E_STRICT only reports major errors. The warnings and notices can get annoying. If you want it to explicitly show everything
*   then set it to E_ALL. 
*
*	IMPORTANT: When the site is finished, set error_reporting(0). This is so Nothing shows up as erroneous in our final site.
*/
//error_reporting(E_ALL);

/**
* This switch is where the content will be displayed. Unless we choose to change it.
* Instructions:
*	- To access a page we would type "domain.com/index.php?page=<switch_case>"
*		-- The '?' Character denotes a "Query" using the $_GET Superglobal. (These can be chained together).
*		-- "page" is the $_GET variable that handles the switch statement. 
*	- The best idea here is to probably write custom PHP functions to output the content.
*		-- I've used this method before, and I do with a lot of pages. It's a very efficient way of displaying content without 
*		   actually changing the page we're looking at.
*/

if( $_SESSION['authenticated'] ){

	switch($_GET['page']){	
		
		case "modifyUser":
		$userName = $_SESSION['id'];
			getUserUpdateData($con, $userName);
			if( isset( $_POST['updateUser'] ) ){
				$pword 				= $_POST['passTwo'];
				$country 			= $_POST['country'];
				$prov_state 		= $_POST['province'];
				$unit 				= $_POST['unitNum'];
				$houseNum 			= $_POST['houseNum'];
				$street				= $_POST['street'];
				$city 				= $_POST['city'];
				$streetDirection 	= $_POST['streetDirection'];
				$postal 			= $_POST['postal'];

	  			updateUserQuery($con, $userName, $pword, $country, $prov_state, $unit, $houseNum, $street, $city, $streetDirection, $postal);
				drawUpdateUserForm($userName, $pass, $country, $prov, $unit, $houseNum, $street, $city, $direction, $postal );				
			
			}
			else{
				$uname 		= $_SESSION['id'];
				$pass 		= $_SESSION['pass'];
				$country 	= $_SESSION['country'];
				$prov 		= $_SESSION['prov'];
				$unit 		= $_SESSION['unit'];
				$houseNum 	= $_SESSION['houseNum'];
				$street 	= $_SESSION['street'];
				$city 		= $_SESSION['city'];
				$direction 	= $_SESSION['direction'];
				$postal 	= $_SESSION['postal'];
						
				drawUpdateUserForm($userName, $pass, $country, $prov, $unit, $houseNum, $street, $city, $direction, $postal );
			}

		
		break;
		
		case "deleteUser":
			$userName = $_SESSION['id'];
			if( isset($_POST['deleteUser']) ){
				
				echo $userName;
				deleteUserQuery($con, $userName);
			}
			else{
				drawUserDeletePage($userName);
			}
			//header("index.php");

		break;

		case "viewUser":
			echo drawUserPage($con);

		break;

	case "viewAuction":
			echo "<h1>Auctions</h1>";
			echo drawAuctions($con);


		break;

	case "viewActiveAuction":;
		echo getActiveSellerAuctions($con);

	break;

	case "deleteAuction":;
		$auctionID = $_GET['auction'];
		drawAuctionDeletePage($con, $auctionID);
		if( isset( $_POST['deleteAuction'] ) ){
			deleteAuctionQuery($con, $auctionID);
			echo drawAuctions($con);
		}

	break;

	case "updateAuction":
            $auctionID = $_GET['auction'];
            getAuctionUpdateData($con, $auctionID);
 
            if( isset( $_POST['updateAuction'] ) )
            {
                $auctionID    	= $_SESSION['auctionID'];
                $sellerID    	= $_SESSION['sellerID'];
                $auctionStart   = $_SESSION['auctionStart'];
                $auctionEnd   	= $_SESSION['auctionEnd'];
                $reserve    	= $_POST['reserve'];
                $status        	= $_SESSION['status'];
                $title        	= $_SESSION['title'];
                $description    = $_POST['description'];
                $winnerID    	= $_SESSION['winnerID'];
                $bidAmount    	= $_SESSION['bidAmount'];
                $bidID        	= $_SESSION['bidID'];
                $buyerID    	= $_SESSION['buyerID'];
 
                $bid        = false;
                updateAuctionQuery($con, $auctionID, $sellerID, $auctionStart, $auctionEnd, $reserve, $status, $title, $description, $winnerID, $bidAmount, $bidID, $buyerID, $bid);
                drawUpdateAuctionForm($con, $auctionID, $sellerID, $auctionStart, $auctionEnd, $reserve, $status, $title, $description, $winnerID, $bidAmount, $bidID, $buyerID);
            }
            else if( isset( $_POST['updateBid'] ) )
            {
                $auctionID    = $_SESSION['auctionID'];
                $sellerID    = $_SESSION['sellerID'];
                $auctionStart    = $_SESSION['auctionStart'];
                $auctionEnd    = $_SESSION['auctionEnd'];
                $reserve    = $_SESSION['reserve'];
                $status        = $_SESSION['status'];
                $title        = $_SESSION['title'];
                $description    = $_SESSION['description'];
                $winnerID    = $_SESSION['winnerID'];
                $bidAmount    = $_SESSION['bidAmount'];
                $bidID        = $_SESSION['bidID'];
                $buyerID    = $_SESSION['buyerID'];
               
                $bid         = true;
                updateAuctionQuery($con, $auctionID, $sellerID, $auctionStart, $auctionEnd, $reserve, $status, $title, $description, $winnerID, $bidAmount, $bidID, $buyerID, $bid);
//                goto Auction page.
//                drawUpdateAuctionForm($con, $auctionID, $sellerID, $auctionStart, $auctionEnd, $reserve, $status, $title, $description, $winnerID, $bidAmount, $bidID, $buyerID);
            }
            else
            {  
                $auctionID    = $_SESSION['auctionID'];
                $sellerID    = $_SESSION['sellerID'];
                $auctionStart    = $_SESSION['auctionStart'];
                $auctionEnd    = $_SESSION['auctionEnd'];
                $reserve    = $_SESSION['reserve'];
                $status        = $_SESSION['status'];
                $title        = $_SESSION['title'];
                $description    = $_SESSION['description'];
                $winnerID    = $_SESSION['winnerID'];
                $bidAmount    = $_SESSION['bidAmount'];
                $bidID        = $_SESSION['bidID'];
                $buyerID    = $_SESSION['buyerID'];
                drawUpdateAuctionForm($con, $auctionID, $sellerID, $auctionStart, $auctionEnd, $reserve, $status, $title, $description, $winnerID, $bidAmount, $bidID, $buyerID);                
            }
        break;

        /*case "checkBids":



        break;*/

        case "viewAuctions":
        	echo viewAuctions($con);
        break;

        case "bid":
        	$auctionID = $_GET['aid'];
        	$buyerID = $_GET['buyerid'];
        	$username = $_GET['id'];
        	$minBid = $_SESSION['minBid'];
        	$auctionTitle = getAuctionName($con, $auctionID);
        	if( isset($_POST['placeBid']) ){
        		$tempBid = $_POST['bidAmount'];
        		if($tempBid >= $minBid){
        			placeBid($con, $tempBid, $auctionID, $buyerID );        			
        		}
        		else{
        			echo "Your bid must be higher than the current maximum bid.";
        			echo bidForm($con, $auctionTitle);
        		}

        	}
        	else{
				echo bidForm($con, $auctionTitle);

        	}

        	
        break;

        case "createAuction":
        	$item = $_POST['itemName'];
        	$reser = $_POST['reserve'];
        	$desc = $_POST['itemDescription'];
        	$dur = $_POST['duration'];
        	$start = $_POST['startDate'];
        	$tempDate = $start;
        	$tempDate = strtotime($start."+ ".$dur." days");
        	$endTime = date("Y/m/d H:i:s",$tempDate);

        	$userName = $_SESSION['id'];   	

        	if( isset($_POST['createAuction']) ){	
				insertAuction($con, $userName, $start, $endTime, $reser, NULL, $item, $desc, NULL);	
			}
			else{
				echo createAuctionForm($con);	
			}
			
			

        break;



		default:
			echo "<h1>Welcome Back ".$_SESSION['id']."!</h1>";
			$userType = isUserOrSeller($con, $username);
			$ID = getbuyerOrsellerID($con, $username);

			if($userType == "Buyer"){
				echo drawBuyerHome($con, $ID);
			}
			else{
				echo drawSellerHome($con, $ID);
			}
			echo "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum diam sit amet sapien imperdiet pulvinar. Nullam vestibulum vulputate velit vitae vehicula. Vivamus pretium quis felis sit amet commodo. Suspendisse rhoncus bibendum nibh eu blandit. Phasellus a porta ligula. Morbi euismod felis in magna dapibus, in hendrerit ante iaculis. Sed lacinia felis et dolor euismod venenatis. Ut ultricies risus tincidunt laoreet facilisis. Vivamus vitae sapien id urna condimentum porta eget non dolor. Nunc sed rutrum purus. Donec aliquet magna sed dolor placerat consectetur. Donec in ipsum pellentesque, convallis odio ut, dapibus diam. Cras rutrum orci eu diam vulputate semper. Aliquam erat volutpat. </p>";
			


	
	
	} // end switch
	
} // end authentication if

else{ // Else the user us not authenticated
	switch($_GET['page']){
	case "createUser":
			//:: NOTE: ::\\
			/*You notice that in the parameters we have Strings that look like " ' ' "
			  We double Quote "" because we are calling a PHP String. We Single Quote inside 
			  the double quote so that the String will also contain ' ' so the Database recognizes 
			  attribute from value.
			*/
			if( isset($_POST['insertUser']) ){
				$uname 				= $_POST['username'];
				$pword 				= $_POST['passOne'];
				$country 			= $_POST['country'];
				$prov_state 		= $_POST['province'];
				$unit 				= $_POST['unitNum'];
				$houseNum 			= $_POST['houseNum'];
				$street				= $_POST['street'];
				$city 				= $_POST['city'];
				$streetDirection 	= $_POST['streetDirection'];
				$postal 			= $_POST['postal'];
				$userType           = $_POST['accountType'];			
						
				insertUser($con, $uname, $pword, $country, $prov_state, $unit, $houseNum, $street, $city, $streetDirection, $postal);

				if($userType == "Buyer"){
					//echo $userType;
					insertBuyer($con, $uname);
				}
				if($userType == "Seller"){
					//echo $userType;
					insertSeller($con, $uname);
				}

				//drawUserForm();	
				
			
			}
			else{
				drawUserForm($uname, $pword, $country, $prov_state, $unit, $houseNum, $street, $city, $streetDirection, $postal);
			}
			
			break;

			case "viewAuction":
				echo "<h1>Auctions</h1>";
				//echo drawAuctions($con);
				echo showAuctionsNoAuth($con);

        	break;

	default:
	echo "<h1>Welcome to BidHouse International</h1>";
	echo "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum diam sit amet sapien imperdiet pulvinar. Nullam vestibulum vulputate velit vitae vehicula. Vivamus pretium quis felis sit amet commodo. Suspendisse rhoncus bibendum nibh eu blandit. Phasellus a porta ligula. Morbi euismod felis in magna dapibus, in hendrerit ante iaculis. Sed lacinia felis et dolor euismod venenatis. Ut ultricies risus tincidunt laoreet facilisis. Vivamus vitae sapien id urna condimentum porta eget non dolor. Nunc sed rutrum purus. Donec aliquet magna sed dolor placerat consectetur. Donec in ipsum pellentesque, convallis odio ut, dapibus diam. Cras rutrum orci eu diam vulputate semper. Aliquam erat volutpat. </p>";
		}
}

switch( $_GET['action'] ){
	case "logout":
		logout();
	
	break;
	
}






?>
